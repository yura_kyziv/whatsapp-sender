<?php
declare(strict_types=1);

namespace Mastering\WhatsAppSender\Api\Data;

interface StoreConfigInterface
{
    const XML_PATH = 'sales/whatsapp_api/';

    const ACTIVE = 'active';
    const TOKEN = 'token';
    const INSTANCE_ID = 'instance_id';
}
