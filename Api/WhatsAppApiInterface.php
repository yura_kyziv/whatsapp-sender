<?php
declare(strict_types=1);

namespace Mastering\WhatsAppSender\Api;

use UltraMsg\WhatsAppApi as WhatsAppApiSender;
use Magento\Sales\Api\Data\OrderInterface;

interface WhatsAppApiInterface
{
    /**
     * @param OrderInterface $order
     * @return WhatsAppApiSender
     */
    public function sendRequest(OrderInterface $order): WhatsAppApiSender;
}
