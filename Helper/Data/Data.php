<?php
declare(strict_types=1);

namespace Mastering\WhatsAppSender\Helper\Data;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Mastering\WhatsAppSender\Api\Data\StoreConfigInterface;

class Data extends AbstractHelper
{

    /**
     * @return mixed
     */
    public  function getActive()
    {
        return $this->scopeConfig->getValue(StoreConfigInterface::XML_PATH . StoreConfigInterface::ACTIVE, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public  function getToken()
    {
        return $this->scopeConfig->getValue(StoreConfigInterface::XML_PATH . StoreConfigInterface::TOKEN, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return mixed
     */
    public  function getInstanceId()
    {
        return $this->scopeConfig->getValue(StoreConfigInterface::XML_PATH . StoreConfigInterface::INSTANCE_ID, ScopeInterface::SCOPE_STORE);
    }

}
