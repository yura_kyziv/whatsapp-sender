<?php
declare(strict_types=1);

namespace Mastering\WhatsAppSender\Helper;

use Magento\Sales\Api\Data\OrderInterface;

class OrderProcessor
{

    /**
     * @param OrderInterface $order
     * @return string
     */
    public function getTelephone(OrderInterface $order): string
    {
        $phoneNumber = $order->getAddresses()[0]->getTelephone();
        $phoneNumber = "+" . $phoneNumber;
        return $phoneNumber;
    }

    /**
     * @param OrderInterface $order
     * @return string
     */
    public function getStatus(OrderInterface $order): string
    {
        $status = $order->getStatus();
        return $status;
    }

    /**
     * @param OrderInterface $order
     * @return string
     */
    public function getOrderMessage(OrderInterface $order): string
    {
        if ($order->getCreatedAt() == $order->getUpdatedAt()){
            $body = "You get new order: ";
        } else {
            $body = "Your order was update: ";
        }
        $body .= $this->getOrderInformation($order);
        return $body;
    }

    /**
     * @param OrderInterface $order
     * @return string
     */
    private function getOrderInformation(OrderInterface $order): string
    {
        $result = "\nOrder ID: {$order->getEntityId()}";
        $result .= "\nStatus: {$order->getStatus()}";
        $result .= "\nPrice: {$order->getGrandTotal()}";
        return $result;
    }
}