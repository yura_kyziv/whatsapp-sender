<?php
declare(strict_types=1);

namespace Mastering\WhatsAppSender\Model;

use Mastering\WhatsAppSender\Api\WhatsAppApiInterface;
use Mastering\WhatsAppSender\Helper\Data\Data;
use Mastering\WhatsAppSender\Helper\OrderProcessor;
use UltraMsg\WhatsAppApi as WhatsAppApiSender;
use Magento\Sales\Api\Data\OrderInterface;

class WhatsAppApi implements WhatsAppApiInterface
{
    /**
     * @var Data
     */
    private Data $helper;

    /**
     * @var OrderProcessor
     */
    private OrderProcessor $orderProcessor;

    /**
     * @param Data $helper
     * @param OrderProcessor $orderProcessor
     */
    public function __construct(
        Data $helper,
        OrderProcessor $orderProcessor
    ){
        $this->orderProcessor = $orderProcessor;
        $this->helper = $helper;
    }

    /**
     * @return WhatsAppApiSender
     */
    private function getApi(): WhatsAppApiSender
    {
        $token = $this->helper->getToken();
        $instance_id = $this->helper->getInstanceId();
        $api = new WhatsAppApiSender($token,$instance_id);
        return $api;
    }

    /**
     * @param OrderInterface $order
     * @return WhatsAppApiSender
     */
    public function sendRequest(OrderInterface $order): WhatsAppApiSender
    {
        $phoneNumber = $this->orderProcessor->getTelephone($order);
        $body = $this->orderProcessor->getOrderMessage($order);
        $api = $this->getApi()->sendChatMessage($phoneNumber, $body);
        return $api;
    }
}
