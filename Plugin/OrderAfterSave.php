<?php
declare(strict_types=1);

namespace Mastering\WhatsAppSender\Plugin;

use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Mastering\WhatsAppSender\Helper\Data\Data;
use Mastering\WhatsAppSender\Api\WhatsAppApiInterface;

class OrderAfterSave
{

    /**
     * @var WhatsAppApiInterface
     */
    private WhatsAppApiInterface $api;

    /**
     * @var Data
     */
    private Data $helper;

    /**
     * @param WhatsAppApiInterface $api
     * @param Data $helper
     */
    public function __construct(
        WhatsAppApiInterface $api,
        Data $helper
    ){
        $this->api = $api;
        $this->helper = $helper;
    }

    /**
     * @param OrderRepositoryInterface $orderRepo
     * @param OrderInterface $order
     * @return OrderInterface
     */
    public function afterSave(OrderRepositoryInterface $orderRepo, OrderInterface $order): OrderInterface
    {
        if($this->helper->getActive()){
            $this->api->sendRequest($order);
        }
        return $order;
    }
}

